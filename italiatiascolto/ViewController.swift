//
//  ViewController.swift
//  italiatiascolto
//
//  Created by emiliano on 19/03/2020.
//  Copyright © 2020 italiatiascolto. All rights reserved.
//

import UIKit
import FirebaseUI.FirebaseFirestoreUI

class ViewController: UIViewController {
    
    enum ViewControllerSegue: String {
        case eventDetail
        case addEvent
    }
    
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var handlerAuth:AuthStateDidChangeListenerHandle? = nil
    var dataSource: FUIFirestoreTableViewDataSource? = nil
    
    var firstAppearance = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let expSeconds = AppConfig.getEventListExpiredMinute().doubleValue * -60.0
        let db = Firestore.firestore()
        let q = db.collection("events").whereField( "startDateTime", isGreaterThanOrEqualTo:Date().addingTimeInterval(expSeconds)).order(by: "startDateTime")
        
        dataSource = tableView.bind(toFirestoreQuery: q, populateCell: { (tv, ip, document) -> UITableViewCell in
            if let cell = tv.dequeueReusableCell(withIdentifier: EventListTableViewCell.Identifier, for: ip) as? EventListTableViewCell {
                let model = EventModel( document:document)
                cell.configure( event: model)
                return cell
            }
            return UITableViewCell()
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(true, animated: true)
        
        handlerAuth = Auth.auth().addStateDidChangeListener {  [weak self] (auth, user) in
            if auth.currentUser == nil {
                self?.avatarImage.image = UIImage(named: "avatar")
            } else if let user = user {
                if let url = user.photoURL {
                    self?.avatarImage.load(url: url)
                }
                if let name = user.displayName, let show = self?.firstAppearance, show {
                    self?.showSuccesfulMessage(String.localizedStringWithFormat("Ciao %@, grazie per esserti unito alla nostra community", name))
                    self?.firstAppearance = false
                }
            }
        }
        
        if let ip = tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: ip, animated: true)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);

        if let handlerAuth = handlerAuth {
            Auth.auth().removeStateDidChangeListener(handlerAuth)
            self.handlerAuth = nil
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier,
                let identifierCase = ViewController.ViewControllerSegue(rawValue: identifier) else {
            assertionFailure("Could not map segue identifier -- (segue.identifier) -- to segue case")
            return
        }

        switch identifierCase {
        case .eventDetail:
            if let vc = segue.destination as? EventDetailViewController,
                let i = tableView.indexPathForSelectedRow?.row,
                let doc =  dataSource?.items[i] {
                vc.eventModel = EventModel( document: doc)
            }
        case .addEvent:break;
        }
    }

    @IBAction func avatarAction(_ sender: Any) {
        if Auth.auth().currentUser == nil {
            let authenticator = AuthenticatorController()
            authenticator.authenticate(from: self)
        } else {
            let optionMenu = UIAlertController(title: nil, message: Auth.auth().currentUser?.email, preferredStyle: .actionSheet)
                
            let deleteAction = UIAlertAction(title: NSLocalizedString("Esci", comment: ""), style: .default) { (alert) in
                if !AuthenticatorController().logout() {
                    self.showErrorMessage(NSLocalizedString("Si è verificato un problema", comment: ""))
                }
            }
            
            let saveAction = UIAlertAction(title: NSLocalizedString("Elimina Account", comment: ""), style: .destructive) { (alert) in
                AuthenticatorController().purge { (error) in
                    if let error = error {
                        self.showErrorMessage(error.localizedDescription)
                    } else {
                        self.showSuccesfulMessage(NSLocalizedString("Log-out effettuato", comment: ""))
                    }
                }
            }
            let cancelAction = UIAlertAction(title: NSLocalizedString("Annulla", comment: ""), style: .cancel)
                
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(saveAction)
            optionMenu.addAction(cancelAction)
                
            self.present(optionMenu, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func addEventAction(_ sender: Any) {
        if Auth.auth().currentUser == nil {
            // TODO: some nice message to explain and then authenticate
            AuthenticatorController().authenticate(from: self)
        } else {
            self.performSegue(withIdentifier: ViewControllerSegue.addEvent.rawValue, sender: self)
        }
    }
}


extension ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: ViewControllerSegue.eventDetail.rawValue, sender: self)
    }
}
