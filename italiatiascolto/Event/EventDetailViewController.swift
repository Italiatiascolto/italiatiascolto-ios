//
//  EventDetailViewController.swift
//  italiatiascolto
//
//  Created by emiliano on 23/03/2020.
//  Copyright © 2020 italiatiascolto. All rights reserved.
//

import UIKit
import TransitionButton

class EventDetailViewController: UITableViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var authorLbl: UILabel!
    @IBOutlet weak var topicLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var partecipantLbl: UILabel!
    @IBOutlet weak var textLbl: UITextView!
    @IBOutlet var joinBtn: TransitionButton!
    
    var eventModel : EventModel? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(false, animated: true)

        
        let df = DateFormatter()
        df.dateStyle = .medium
        df.timeStyle = .short
        
        authorLbl.text = eventModel?.author
        topicLbl.text = eventModel?.topicName
        titleLbl.text = eventModel?.title

        if let value = eventModel?.duration {
            durationLbl.text = String.localizedStringWithFormat( "%d minuti", value)
        } else {
            durationLbl.text = "-"
        }
        
        if let value = eventModel?.startDateTime as Date? {
            timeLbl.text = df.string(from: value)
        } else {
            timeLbl.text = "-"
        }
        
        if let max = eventModel?.maxPartecipants, let expected = eventModel?.partecipants.count {
            let n = max - expected
            if n == 0 {
                partecipantLbl.text = NSLocalizedString("Non ci sono più posti disponibili", comment: "")
                joinBtn.isHidden = true
            } else {
                partecipantLbl.text = String.localizedStringWithFormat( "%d posti ancora dispinibili", max - expected)
            }
        } else {
            partecipantLbl.text = NSLocalizedString("Evento aperto a tutti", comment: "")
        }
        
        if let value = eventModel?.topicID {
            self.imageView.image = UIImage( named: value)
        } else {
            self.imageView.image = nil
        }
        self.textLbl.text = eventModel?.notes
       // tableView.addSubview( joinBtn)
    }
    
    @IBAction func infoAction(_ sender: Any) {
    }
}
