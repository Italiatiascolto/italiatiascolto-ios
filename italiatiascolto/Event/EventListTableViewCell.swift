//
//  EventListTableViewCell.swift
//  italiatiascolto
//
//  Created by emiliano on 20/03/2020.
//  Copyright © 2020 italiatiascolto. All rights reserved.
//

import UIKit


class EventListTableViewCell: UITableViewCell {

    static public let Identifier = "eventCell"

    @IBOutlet weak var footerLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var imageIconView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func configure( event:EventModel) {

        self.titleLabel?.text = event.title
        self.footerLabel?.text = event.author
        
        let df = DateFormatter()
        df.dateStyle = .short
        df.timeStyle = .short
        df.doesRelativeDateFormatting = true;

        self.captionLabel.text = df.string(from: event.startDateTime)
        
        self.imageIconView.image = UIImage( named: event.topicID)
    }

}
