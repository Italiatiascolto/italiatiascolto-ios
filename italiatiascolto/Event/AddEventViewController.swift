//
//  AddEventViewController.swift
//  italiatiascolto
//
//  Created by emiliano on 19/03/2020.
//  Copyright © 2020 italiatiascolto. All rights reserved.
//
import UIKit
import Former
import TransitionButton
import FirebaseFirestore
import FirebaseFirestoreSwift



final class AddEventViewController: FormViewController {
    @IBOutlet weak var saveBtn: TransitionButton!
    
    private var dataModel:EventModel = EventModel()
    private var topics:[TopicModel] = []

    // MARK: Public
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isModalInPresentation = true
        let closeBtn = UIButton(type: .close)
        closeBtn.frame = CGRect(x: self.view.frame.width - 44, y: 10, width: 33, height: 33)
        closeBtn.addTarget(self, action: #selector(closeAction(_:)), for: .touchUpInside)
        view.addSubview(closeBtn)
        
        saveBtn.startAnimation()
        TopicModel.fetch { (topics, error) in
            self.saveBtn.stopAnimation()
            
            if let error = error {
                self.showErrorMessage(error.localizedDescription)
                self.dismiss(animated: true, completion: nil)
            } else {
                self.topics = topics
                self.configure()
            }
        }
    }
    
    private func isFormValid() -> Bool {
        if dataModel.title.count < 5 {
            return false
        }
        if dataModel.duration < 15 {
            return false
        }
        if dataModel.maxPartecipants < 1 {
            return false
        }
        if dataModel.startDateTime < Date() {
            return false
        }
        return true
    }
    

    @IBAction func saveAction(_ sender: Any) {
        if !isFormValid() {
            self.saveBtn.shake( duration: 1)
            return
        }
        if let uid = AuthenticatorController().userID() {
            self.saveBtn.startAnimation()
            let db = Firestore.firestore()
            let data = [
                    "author": AuthenticatorController().userLabel(),
                    "authorID": uid,
                    "created": Date(),
                    "title": dataModel.title,
                    "topicID": dataModel.topicID,
                    "topicName": dataModel.topicName,
                    "startDateTime": dataModel.startDateTime,
                    "repeatEvent": dataModel.repeatEvent,
                    "duration": dataModel.duration,
                    "maxPartecipants": dataModel.maxPartecipants,
                    "partecipants": [],
                    "notes": dataModel.notes] as [String : Any]
            
            db.collection("events").addDocument(data:data) { err in
                if let err = err {
                    self.showErrorMessage(err.localizedDescription)
                    self.saveBtn.stopAnimation(animationStyle: .shake, revertAfterDelay: 1, completion: nil)
                } else {
                    self.saveBtn.stopAnimation(animationStyle: .expand, revertAfterDelay: 2) {
                        self.dismiss(animated: false, completion: nil)
                    }
                }
            }
        }
    }

    // MARK: Private
    @objc func closeAction(_ sender:UIButton!) {
        self.dismiss(animated: true) {}
    }

    private static func DurationValues() -> [Int] {
        return [15,30,45,60]
    }
    
    private static func PartecipantsValues() -> [Int] {
        return [1,2,3,4,5,6,7,8,9,10,12,13,14,15]
    }

    private enum Repeat {
        case Never, Daily, Weekly, Monthly
        func title() -> String {
            switch self {
                case .Never: return NSLocalizedString("Mai", comment: "")
                case .Daily: return NSLocalizedString("Giornalmente", comment: "")
                case .Weekly: return NSLocalizedString("Settimanalmente", comment: "")
                case .Monthly: return NSLocalizedString("Mensilmente", comment: "")
            }
        }
        static func values() -> [Repeat] {
            return [Daily, Weekly, Monthly]
        }
    }

    private func configure() {
        title =  NSLocalizedString("Aggiungi Evento", comment: "")
        tableView.contentInset.top = 30
        tableView.contentInset.bottom = 30
        tableView.contentOffset.y = 0
        // Create RowFomers
        let titleRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.textField.font = .systemFont(ofSize: 15)
        }.configure {
            $0.placeholder = NSLocalizedString("Argomento", comment: "")
        }.onTextChanged {
            [weak self] (value) in
            self?.dataModel.title = value
        }
        
        let themeRow = InlinePickerRowFormer<FormInlinePickerCell, String>() {
            $0.titleLabel.text = NSLocalizedString("Categoria", comment: "")
            $0.titleLabel.font = .boldSystemFont(ofSize: 15)
            $0.displayLabel.font = .systemFont(ofSize: 15)
        }.configure {
            $0.pickerItems = self.topics.map {
                InlinePickerItem(title: $0.name, value: $0.ID)
            }
            if let def = self.topics.first {
                self.dataModel.topicID = def.ID
                self.dataModel.topicName = def.name
            }
        }.onValueChanged {
            [weak self] in
            if let value = $0.value {
                self?.dataModel.topicID = value
            }
            self?.dataModel.topicName = $0.title
        }
    
        let partecipants = InlinePickerRowFormer<FormInlinePickerCell, Int>(instantiateType: .Class) {
             $0.titleLabel.text = NSLocalizedString("Limita Partecipanti", comment: "")
             $0.titleLabel.font = .boldSystemFont(ofSize: 15)
             $0.displayLabel.font = .systemFont(ofSize: 15)
         }.configure {
             $0.pickerItems = AddEventViewController.PartecipantsValues().enumerated().map {
                 InlinePickerItem(title: String.localizedStringWithFormat("%d", $0.element), value: AddEventViewController.PartecipantsValues()[$0.offset])
             }
             $0.selectedRow = 4
            dataModel.maxPartecipants = 5;
         }.onValueChanged {
             [weak self] in
             if let value = $0.value {
                 self?.dataModel.maxPartecipants = value
             }
         }
        
        let startRow = InlineDatePickerRowFormer<FormInlineDatePickerCell>() {
            $0.titleLabel.text = NSLocalizedString("Inizio", comment: "")
            $0.titleLabel.font = .boldSystemFont(ofSize: 15)
            $0.displayLabel.font = .systemFont(ofSize: 15)
        }.inlineCellSetup {
            $0.datePicker.datePickerMode = .dateAndTime
            $0.datePicker.minuteInterval = 15
            $0.datePicker.minimumDate = Date().addingTimeInterval(60.0*35.0)
        }.configure { (picker) in
            let date = Date(timeIntervalSinceReferenceDate:( (Date.timeIntervalSinceReferenceDate / 3600.0).rounded(.toNearestOrEven) * 3600.0) + (3600.0 * 23.0))
            picker.date = date
            dataModel.startDateTime = date;
        }.onDateChanged({
            [weak self] (date) in
            self?.dataModel.startDateTime = date
        }).displayTextFromDate(String.mediumDateShortTime)

        let endRow = InlinePickerRowFormer<FormInlinePickerCell, Int>(instantiateType: .Class) {
            $0.titleLabel.text = NSLocalizedString("Durata", comment: "")
            $0.titleLabel.font = .boldSystemFont(ofSize: 15)
            $0.displayLabel.font = .systemFont(ofSize: 15)
        }.configure {
            $0.pickerItems = AddEventViewController.DurationValues().enumerated().map {
                InlinePickerItem(title: String.localizedStringWithFormat("%d Minuti", $0.element), value: AddEventViewController.DurationValues()[$0.offset])
            }
            $0.selectedRow = 3
       //     $0.displayEditingColor = .formerHighlightedSubColor()
        }.onValueChanged {
            [weak self] in
            if let value = $0.value {
                self?.dataModel.duration = value
            }
        }

        let repeatRow = InlinePickerRowFormer<FormInlinePickerCell, Repeat>() {
            $0.titleLabel.text = NSLocalizedString("Ricorrente", comment: "")
            $0.titleLabel.font = .boldSystemFont(ofSize: 15)
            $0.displayLabel.font = .systemFont(ofSize: 15)
        }.configure {
            let never = Repeat.Never
            $0.pickerItems.append(
                InlinePickerItem(title: never.title(),
                                 displayTitle: NSAttributedString(string: never.title(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray]),
                                 value: never))
            $0.pickerItems += Repeat.values().map {
                InlinePickerItem(title: $0.title(), value: $0)
            }
        }.onValueChanged {
             [weak self] in
             if let value = $0.value {
                self?.dataModel.repeatEvent = value.hashValue
             }
        }

        let noteRow = TextViewRowFormer<FormTextViewCell>() {
            $0.textView.font = .systemFont(ofSize: 15)
        }.configure {
            $0.placeholder = NSLocalizedString("Note", comment: "")
            $0.rowHeight = 150
        }.onTextChanged {
            [weak self] (value) in
            self?.dataModel.notes = value
        }

        // Create Headers
        let createHeader: (() -> ViewFormer) = {
            return CustomViewFormer<FormHeaderFooterView>()
            .configure {
                $0.viewHeight = 20
            }
        }

        // Create SectionFormers
        let titleSection = SectionFormer(rowFormer: titleRow, themeRow,partecipants)
        .set(headerViewFormer: createHeader())
        let dateSection = SectionFormer(rowFormer: startRow, endRow)
        .set(headerViewFormer: createHeader())
        let repeatSection = SectionFormer(rowFormer: repeatRow)
        .set(headerViewFormer: createHeader())
        let noteSection = SectionFormer(rowFormer: noteRow)
        .set(headerViewFormer: createHeader())
        former.append(sectionFormer: titleSection, dateSection, repeatSection, noteSection)
    }
}
extension String {
    static func shortTime(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .current
        dateFormatter.timeStyle = .short
        dateFormatter.dateStyle = .none
        return dateFormatter.string(from: date)
    }

    static func mediumDateShortTime(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .current
        dateFormatter.timeStyle = .short
        dateFormatter.dateStyle = .medium
        return dateFormatter.string(from: date)
    }

    static func mediumDateNoTime(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .current
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .medium
        return dateFormatter.string(from: date)
    }

    static func fullDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .current
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .full
        return dateFormatter.string(from: date)
    }

}
