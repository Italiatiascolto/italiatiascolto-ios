//
//  MyEventsViewController.swift
//  italiatiascolto
//
//  Created by emiliano on 19/03/2020.
//  Copyright © 2020 italiatiascolto. All rights reserved.
//

import UIKit
import FirebaseAuth

class MyEventsViewController: UIViewController {

    var handlerAuth:AuthStateDidChangeListenerHandle? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        handlerAuth = Auth.auth().addStateDidChangeListener { (auth, user) in
            
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);
        
        if let handlerAuth = handlerAuth {
            Auth.auth().removeStateDidChangeListener(handlerAuth)
            self.handlerAuth = nil
        }

    }

    @IBAction func loginAction(_ sender: Any) {
        
        let auth = AuthenticatorController()
        auth.authenticate(from: self)
        
    }
}
