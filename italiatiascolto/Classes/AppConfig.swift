//
//  AppConfig.swift
//  italiatiascolto
//
//  Created by emiliano on 20/03/2020.
//  Copyright © 2020 italiatiascolto. All rights reserved.
//

import Foundation
import Firebase

class AppConfig {
    static let eventsExpiredMinutesConfigKey = "list_events_expired_from"

    static public func AppInit() {

        FirebaseApp.configure()

        let remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.setDefaults(fromPlist: "RemoteConfigDefaults")
    }
    
    public static func getEventListExpiredMinute() -> NSNumber {
        return RemoteConfig.remoteConfig().configValue(forKey: AppConfig.eventsExpiredMinutesConfigKey).numberValue ?? 15
    }
}
