//
//  TopicModel.swift
//  italiatiascolto
//
//  Created by emiliano on 20/03/2020.
//  Copyright © 2020 italiatiascolto. All rights reserved.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift

struct TopicModel {
    let ID:String
    let name:String
    
    static func fetch( completition: @escaping([TopicModel], Error?) -> ()) {
        let db = Firestore.firestore()
        db.collection("topics").getDocuments() { (querySnapshot, err) in
           let topics:[TopicModel]
           if err != nil {
               topics = []
           } else {
                topics = querySnapshot!.documents.compactMap({ (doc) -> TopicModel in
                    let name:String
                    if let value = doc.data()["name"] as? String {
                        name = value
                    } else {
                        name = doc.documentID
                    }
                    return TopicModel( ID: doc.documentID, name: name)
                })
           }
           completition(topics, err)
        }
    }
}
