//
//  EventModel.swift
//  italiatiascolto
//
//  Created by emiliano on 20/03/2020.
//  Copyright © 2020 italiatiascolto. All rights reserved.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift

struct EventModel {
    var title:String = ""
    var notes:String = ""
    var topicID:String = ""
    var topicName:String = ""
    var author:String = ""
    var authorID:String = ""

    var startDateTime:Date = Date()
    var repeatEvent:Int = 0
    var duration:Int = 30
    var maxPartecipants:Int = 10

    var partecipants:[String] = []

    init() {
        
    }

    init(document:DocumentSnapshot) {
        let data = document.data()
        self.authorID = data?["authorID"] as? String ?? ""
        self.author = data?["author"] as? String ?? ""
        self.title = data?["title"] as? String ?? ""
        self.notes = data?["notes"] as? String ?? ""
        self.topicID = data?["topicID"] as? String ?? ""
        self.topicName = data?["topicName"] as? String ?? ""
        self.partecipants = data?["partecipants"] as? [String] ?? []

        if let value = data?["duration"] as? Int {
            self.duration = value
        }
        
        if let value = data?["repeatEvent"] as? Int {
            self.repeatEvent = value
        }
        
        if let value = data?["maxPartecipants"] as? Int {
            self.maxPartecipants = value
        }
        
        if let value =  data?["startDateTime"] as? Timestamp {
            self.startDateTime = value.dateValue() 
        }

 
    }
}
