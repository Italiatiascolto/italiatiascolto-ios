//
//  UIViewController+extra.swift
//  italiatiascolto
//
//  Created by emiliano on 19/03/2020.
//  Copyright © 2020 italiatiascolto. All rights reserved.
//

import BRYXBanner

extension UIViewController {
    func showSuccesfulMessage( _ msg:String, title:String? = nil, image:UIImage? = nil) {
        
        let green = UIColor(red:48.00/255.0, green:174.0/255.0, blue:51.5/255.0, alpha:1.000)
        let banner = Banner(title: title, subtitle:msg, image: image, backgroundColor: green)
        banner.dismissesOnTap = true
        banner.show(duration: 3.0)
    }
    
    func showErrorMessage( _ msg:String, title:String? = nil, image:UIImage? = nil) {
        
        let red = UIColor(red:198.0/255.0, green:26.00/255.0, blue:27.0/255.0, alpha:1.000)
        let banner = Banner(title: title, subtitle:msg, image: image, backgroundColor: red)
        banner.dismissesOnTap = true
        banner.show(duration: 3.0)
    }
}
