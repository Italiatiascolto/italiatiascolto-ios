//
//  VideoRoomViewController.swift
//  italiatiascolto
//
//  Created by emiliano on 19/03/2020.
//  Copyright © 2020 italiatiascolto. All rights reserved.
//

import UIKit

import AgoraRtcEngineKit

class VideoRoomViewController: UIViewController {

    @IBOutlet weak var previewCollectionViewController: UICollectionView!
    @IBOutlet weak var extiBtn: UIButton!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var micBtn: UIButton!
    
    var agoraKit:AgoraRtcEngineKit? = nil
    var isCallRunning:Bool = false
    
    let videoView:UIView = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        agoraKit = AgoraRtcEngineKit.sharedEngine(withAppId: "e90faa65d5284c398aef249a5a86a80f", delegate: self)
        agoraKit?.enableWebSdkInteroperability(true)

        setupLocalVideo()
    }


    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        agoraKit?.enableVideo()
        joinChannel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        agoraKit?.disableVideo()
        leaveChannel()
    }

    func setupLocalVideo() {
        videoView.translatesAutoresizingMaskIntoConstraints = false
        self.view.insertSubview(videoView, at: 0)
        self.videoView.pinEdges(to: self.view)

        let videoCanvas = AgoraRtcVideoCanvas()
        videoCanvas.uid = numericIdForUID( AuthenticatorController().userID())
        videoCanvas.view = self.videoView
        videoCanvas.renderMode = .hidden
        
        // Set the local video view.
        agoraKit?.setupLocalVideo(videoCanvas)
     
        
        
    }
    
    func enableVideo() {
        agoraKit?.enableVideo()
    }
    
 
    
    func joinChannel() {
        let token = ""
    
        let channelName = "demo"
        
        agoraKit?.setDefaultAudioRouteToSpeakerphone(true)

        let uid = numericIdForUID( AuthenticatorController().userID())
        agoraKit?.joinChannel(byToken: token, channelId: channelName, info:nil, uid:uid) { [unowned self] (channel, uid, elapsed) -> Void in
            
        }
        
        //self.logVC?.log(type: .info, content: "did join channel")
        isCallRunning = true
    }

    func leaveChannel() {
        agoraKit?.leaveChannel(nil)
    }
    
    @IBAction func cameraAction(_ sender: Any) {
        agoraKit?.switchCamera()
      //  agoraKit?.disableVideo()
    }
    
    @IBAction func cameraOnOffAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            agoraKit?.disableVideo()
        } else {
            agoraKit?.enableVideo()
        }
    }
    
    @IBAction func microphoneAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        agoraKit?.muteLocalAudioStream(sender.isSelected)
    }
    
    @IBAction func exitAction(_ sender: Any) {
        self.leaveChannel()
        self.dismiss(animated: true, completion: nil)
    }
    
    private func numericIdForUID( _ uid:String?) -> UInt{
        print( "%@", uid ?? "")
        let uidInt:UInt
        if let id = uid, let uidc = UInt(id) {
            uidInt = uidc
        } else {
            uidInt = 0
        }
        return uidInt
    }
}


extension VideoRoomViewController: AgoraRtcEngineDelegate {
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstRemoteVideoDecodedOfUid uid:UInt, size:CGSize, elapsed:Int) {
        /*isRemoteVideoRender = true
        let videoCanvas = AgoraRtcVideoCanvas()
        videoCanvas.uid = uid
        videoCanvas.view = remoteVideo
        videoCanvas.renderMode = .hidden
        // Set the remote video view.
        agoraKit.setupRemoteVideo(videoCanvas*)*/
    }
}


extension UIView {
    func pinEdges(to other: UIView) {
        leadingAnchor.constraint(equalTo: other.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: other.trailingAnchor).isActive = true
        topAnchor.constraint(equalTo: other.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: other.bottomAnchor).isActive = true
    }
}
