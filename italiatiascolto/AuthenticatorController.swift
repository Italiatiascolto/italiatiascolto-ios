//
//  AuthenticatorController.swift
//  italiatiascolto
//
//  Created by emiliano on 19/03/2020.
//  Copyright © 2020 italiatiascolto. All rights reserved.
//

import Foundation
import FirebaseUI

class AuthenticatorController:NSObject {
    
    func authenticate( from:UIViewController) {
        let authUI = FUIAuth.defaultAuthUI()
        
        let providers: [FUIAuthProvider] = [
        //  FUIEmailAuth(),
          FUIGoogleAuth()
        //  FUIFacebookAuth(),
        //  FUIOAuth.appleAuthProvider()
        ]
        authUI?.providers = providers
        authUI?.delegate = self
        
        let authViewController = authUI!.authViewController()
        from.present( authViewController, animated: true);
    }
    
    func logout() -> Bool {
        do {
            try Auth.auth().signOut()
            return true
        } catch {
            return false
        }
    }
    
    func purge( completition: @escaping(Error?) -> ()) {
        let user = Auth.auth().currentUser
        user?.delete { error in
            completition(error)
        }
    }
    
    func userLabel() -> String {
        if let user = Auth.auth().currentUser {
            if let name = user.displayName {
                return name
            }
            return user.uid
        }
        return NSLocalizedString("Anonimo", comment: "")
    }
    
    func userID() -> String? {
        return Auth.auth().currentUser?.uid
    }
}

extension AuthenticatorController: FUIAuthDelegate {
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
      // handle user (`authDataResult.user`) and error as necessary
    }
}
